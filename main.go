package main

import (
  "html/template"
  "log"
  "net/http"
)

func handler(writer http.ResponseWriter, request *http.Request) {
  var resp *http.Response
  var err error

  if request.Method == "POST" {
    uri := "http://" + request.FormValue("name") + "." + request.FormValue("namespace") + ".svc.cluster.local:5000"
    resp, err = http.Get(uri)
  }
  t, _ := template.ParseFiles("ping.html")
  data := struct {
    Response *http.Response
    Error error
  }{
    resp,
    err,
  }
  t.Execute(writer, data)
}

func main() {
  http.HandleFunc("/", handler)
  log.Fatal(http.ListenAndServe(":5000", nil))
}
