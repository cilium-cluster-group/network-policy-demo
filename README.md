# Network Policy Demo

This project provides a way of testing communication between pods through their respective [services](https://kubernetes.io/docs/concepts/services-networking/service/).


It can be used for clusters that are implementing [Cilium](https://docs.cilium.io/) as part of the [cluster application](https://docs.gitlab.com/ee/user/clusters/applications.html#install-using-gitlab-ci-alpha).


### Pre-requisites
One or more projects can share the same cluster and it can be achieve by having the k8s cluster defined in the parent group.

### Name and Namespace
Applications will be deployed in Kubernetes with service names and namespaces not known prior deployment. After querying the desired services (e.g., `kubectl get svc --all-namespaces`), this information can be used as an input to this project.

### NetworkPolicy

The current [network policy](https://kubernetes.io/docs/concepts/services-networking/network-policies/) is set to allow incoming connections from any pod from namespaces with the label `name` set to `gitlab-managed-apps`.
